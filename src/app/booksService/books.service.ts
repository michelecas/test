import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/Rx";

import { environment } from '../../environments/environment';


@Injectable()
export class BooksService {

	private book: number;

	constructor(
		private http: HttpClient
	) { }

	setBook(id: number) {
		this.book = id;
	}

	getBook(): number {
		return this.book;
	}

	getBooksList(success, error) {
		this.get(success, error, environment.url + "api/Books");
	}

	getBookAuthors(success, error, id) {
		this.get(success, error, environment.url + "authors/books/" + id);
	}

	getBookDetail(success, error, id) {
		this.get(success, error, environment.url + "api/books/" + id);
	}

	get(success, error, url) {
		let _call = this.http.get(url);
		this.makeCall(_call, success, error);
	}

	makeCall(call, success, error) {
		call.flatMap(
			(result) => {
				return Observable.of(result);
			}
		).subscribe(
			(result) => {
				success(result);
			},
			(err) => {
				error(err);
			}
		);
	}
}
