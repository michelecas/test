import { TestBed, inject } from '@angular/core/testing';

import { BooksService } from './books.service';
import { HttpClientModule } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/Rx";

describe('BooksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BooksService
      ],
      imports: [
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([BooksService], (service: BooksService) => {
    expect(service).toBeTruthy();
  }));
});
