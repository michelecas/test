import { MatIconModule } from '@angular/material';

import { Component, OnInit } from '@angular/core';

import { BooksService } from '../../booksService/books.service';

import * as moment from 'moment';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

	public currentBookId: number;
	public currentBookData = null;
	public currentBookAuthors = null;

	constructor(
		private booksService: BooksService
	) {
		this.currentBookId = this.booksService.getBook();
		this.initBookData();
	}

	initBookData() {
		this.booksService.getBookDetail(
			(data) => {
				data ? this.currentBookData = data : this.currentBookData = null;
			},
			(err) => {
				console.log(err)
			},
			this.currentBookId
		);
		this.booksService.getBookAuthors(
			(data) => {
				data ? this.currentBookAuthors = data : this.currentBookAuthors = null;
			},
			(err) => {
				console.log(err)
			},
			this.currentBookId
		);
	}

	translateDate(date: string) {
		let res = moment(date).format("DD/MM/YY");
		return res;
	}

	ngOnInit() {
	}

}
