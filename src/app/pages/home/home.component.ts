import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

import { BooksService } from '../../booksService/books.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public booksList;
	public length: number;
	public pageSize: number;
	public currentIndex: number;
	public hValue: number;
	public lValue: number;

	public breakpoints = [
		{
			grid: 1,
			width: 0,
		},
		{
			grid: 2,
			width: 510,
		},
		{
			grid: 4,
			width: 960,
		},
		{
			grid: 6,
			width: 1415,
		}
	];

	public breakpoint: number;

	constructor(
		private booksService: BooksService
	) {
		this.initBooksData();

		this.pageSize = 20;
		this.currentIndex = 0;
		this.hValue = this.pageSize;
		this.lValue = 0;

		this.breakpoint = 4;
	}

	onResize(event) {
		this.breakpoints.forEach(
			(brk) => {
				if(event.target.innerWidth >= brk.width){
					this.breakpoint = brk.grid;
				}
		});
	}

	ngOnInit() { }

	translateDate(date: string) {
		let res = moment(date).format("DD/MM/YY");
		return res;
	}

	initBooksData() {
		this.booksService.getBooksList(
			(data) => {
				this.booksList = data;
				this.length = data.length;
			},
			(err) => {
				console.log(err)
			});
	}

	getPaginatorData(event) {
		if (event.pageIndex == this.currentIndex + 1) {
			this.lValue += this.pageSize;
			this.hValue += this.pageSize;
		}
		else if (event.pageIndex == this.currentIndex - 1) {
			this.lValue -= this.pageSize;
			this.hValue -= this.pageSize;
		}
		this.currentIndex = event.pageIndex;
	}

	setBook(id: number){
		this.booksService.setBook(id);
	}
}
