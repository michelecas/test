import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { BooksService } from '../../booksService/books.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule, MatGridListModule, MatButtonModule, MatPaginatorModule, MatIconModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { DetailsComponent } from '../details/details.component';

import * as moment from 'moment';

describe('HomeComponent', () => {
	let component: HomeComponent;
	let fixture: ComponentFixture<HomeComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				HomeComponent,
				DetailsComponent
			],
			providers: [
				BooksService
			],
			imports: [
				RouterTestingModule.withRoutes([
				  { path: '', redirectTo: '/home', pathMatch: 'full' },
				  { path: 'home', component: HomeComponent },
				  { path: 'book/:id', component: DetailsComponent }
				]),
			  HttpClientModule,
			  BrowserAnimationsModule,
			  HttpClientModule,
			  MatCardModule,
			  MatGridListModule,
			  MatButtonModule,
			  MatPaginatorModule,
			  MatIconModule
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HomeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
		expect(component.pageSize).toBe(20);
		expect(component.currentIndex).toBe(0);
		expect(component.hValue).toBe(component.pageSize);
		expect(component.lValue).toBe(0);
		expect(component.breakpoint).toBe(4);
	});

	describe('onResize', () => {

		it('should resize the grid', () => {
			expect(component.breakpoint).toBe(4);
			component.onResize({ target: { innerWidth: 511 } });
			expect(component.breakpoint).toBe(2);

		});
	});

	describe('onResize', () => {

		it('should return the correct datetime', () => {
			expect(component.translateDate("2018-11-28T14:57:03+01:00")).toBe("28/11/18")
		});
	});

	describe('getPaginatorData', () => {
		it('should return the corrects hig and low items value', () => {
			component.currentIndex = 1;
			component.getPaginatorData({pageIndex: 0})
			expect(component.currentIndex).toBe(0);
			expect(component.currentIndex).not.toBe(1);
		})
	});
});
